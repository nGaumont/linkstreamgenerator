import sys
from itertools import combinations
from src.Activity import Activity
from src import colorGenerator
import json
import random


def usage():
    print("Generate an linkstream according to an affilation graph " +
          "and an activty signature")
    print("Usage: python generator.py out=outFile c=affiliation.txt a=activity.txt t=TMAX")
    print("\t c= affilation format: list of nodeiD ComID1;ComID2;")
    print("\t a= activity format")
    print("\t t= end time of the generated linkstream not used yet")
    print("\t out= output file. if outputFile ends with json " +
          "then it uses linkstreamViz format")
    print()
    exit(1)


def read_argv():
    res = {"c": ""}
    for arg in sys.argv:
        if "=" in arg:
            content = arg.split("=")
            arg_name = content[0].replace("--", "")
            res[arg_name] = content[1]
    return res


def readAffiliation(affiliationFile):
    """ return a dict key = nodeID, values = {ComIDs}"""
    res = {}
    with open(affiliationFile, 'r') as affFile:
        for line in affFile:
            tab = line.split()
            assert(len(tab) == 2)
            nodeID = int(tab[0])
            comID = set(sorted([int(com) for com in tab[1].split(";")]))
            res[nodeID] = comID
    return res


def readActivity(activityFile):
    """ return a dict key = comID, values = Activity"""
    res = {}
    with open(activityFile, 'r') as actFile:
        for line in actFile:
            tab = line.split()
            assert(len(tab) == 2)
            comID = int(tab[0])
            activity = Activity(comID)
            for points in tab[1].split(";"):
                p = points.split(",")
                assert(len(p) == 2)
                activity.addActivty(float(p[0]), float(p[1]))
            res[comID] = activity
    return res

if __name__ == '__main__':
    if "-h" in sys.argv or "--help" in sys.argv or len(sys.argv) < 2:
        usage()
    arg = read_argv()
    comFile = arg.get("c")
    assert(comFile)
    affiliation = readAffiliation(comFile)

    actFile = arg.get("a")
    assert(actFile)
    actList = readActivity(actFile)

    outFile = arg.get("out")
    assert(outFile)

    TMAX = float(arg.get("t"))
    assert(TMAX)

    output = []
    colors = colorGenerator.colorGenerator()

    link_count = 0
    for n1, n2 in combinations(affiliation, 2):
        for com in affiliation[n1].intersection(affiliation[n2]):
            activations = actList[com].activationBetween(0, TMAX)
            last = 0
            for time in activations:
                last = time
                link_count += 1
                output.append({"from": n1,
                               "to": n2,
                               "time": time,
                               "color": colors.get(com),
                               "com": com})
    outputAff = "aff_"+outFile
    output =  sorted(output, key=lambda links: links["time"])
    with open(outFile, 'w') as outfile, open(outputAff, 'w') as outAff:
        if "json" in outFile:
            json.dump(output, outfile, sort_keys=True, indent=4,
                      ensure_ascii=False)
        else:
            for lid, l in enumerate(output):
                link = " ".join((str(l[x]) for x in ["time", "from", "to"]))
                outfile.write(link+"\n")
                outAff.write(str(lid)+" "+str(l["com"])+"\n")
    print(str(link_count)+ " links have been created.")
    print("Links affiliation have been written in "+str(outputAff))
