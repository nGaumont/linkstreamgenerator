Generator.py
===========

This generates a linkstream with a known community structure on the links.  
It use 2 files in input:  
 * an activity file  
 * an affiliation file  

You can also use default.py which has less parameter (use -h option for more information).  


### activity file

This file contains for each community its activity profile.
An activity profile defines the activity parameter of the community over the time.
The parameter is the mean number of activation in one unit time.
The folling line:
```
4 0.1,5;0.2,15;0.3,10;0.1,2
```
means that the community 4 has :  
 * in [0;5[ an activity of 0.1  
 * in [5;20[ an activity of 0.2  
 * in [20;30[ an activity of 0.3  
 * in [30;32[ an activity of 0.1  
 * in [32;infinty[ an null activity.  

An activity of 0.1 means that on average there is 0.1 link per unit time.

### affiliation file

 This file contains for each node the list of community it belongs to.  
 Example of 5 nodes in 3 overlapping community:  
 ```
 0 0;1;2
 1 1;2
 2 0;2
 3 0
 4 1
 ```
