# -*- coding: utf8 -*-
import sys
import math
import bisect
import random


def usage():
    print("Generate default parameter for the linkstream generation.")
    print("Each community will have a unique burst during 'duration' uniformly drawn in [0; TMAX].")
    print("Usage: python default.py nb_node nb_com AvgCom duration activity TMAX")
    print("\t nb_node is the number of nodes (integer)")
    print("\t nb_com is the number of communities (integer)")
    print("\t AvgCom is average number of community per node (float)")
    print("\t duration is the duration of the burst (float)")
    print("\t activity is the activity level of the burst . It's the number of activation per unit time. (float)")
    print("\t TMAX is the end of the linkstream")
    print("\t Duration and activity can be drawn uniformly in arbitrary interval (min-max)")
    print()
    print("Example:")
    print("python default.py 500 100 5 10-30 0.3-0.8 150")
    exit()


def makeIntervale(str_input, sep="-"):
    tmp = [float(x) for x in str_input.split(sep)]
    if len(tmp) == 1:
        tmp.append(tmp[0])
    assert(len(tmp) == 2)
    return tmp


def randInIntervale(interval):
    if interval[0] == interval[1]:
        return interval[0]
    span = interval[1] - interval[0]
    return interval[0] + random.random() * span


if __name__ == '__main__':
    if "-h" in sys.argv or "--help" in sys.argv or len(sys.argv) < 7:
        usage()
    nb_node = int(sys.argv[1])
    nb_com = int(sys.argv[2])
    mean_deg_node = float(sys.argv[3])
    dens = mean_deg_node / nb_com
    nodes = [set() for x in range(0, nb_node)]
    coms = [set() for x in range(0, nb_com)]
    deg_com_moyen = dens*nb_node
    m = dens*nb_node*nb_com
    tire = 0
    # print("Proportion overlap:", avg_overlap)
    print("Taille moyenne d'une com: "+str(deg_com_moyen))
    print("Nombre de communauté par noeuds: "+str(mean_deg_node))
    for nodeID in range(0, nb_node):
        rand_com = random.randint(0, nb_com-1)
        nodes[nodeID].add(rand_com)
        coms[rand_com].add(nodeID)
        tire += 1
    while tire <= m:
        rand_node = random.randint(0, nb_node-1)
        rand_com = random.randint(0, nb_com-1)
        if rand_com not in nodes[rand_node]:
            nodes[rand_node].add(rand_com)
            coms[rand_com].add(rand_node)
            tire += 1
    nb = 0
    overlap = 0
    mean_size = 0
    for i, com in enumerate(coms):
        mean_size += len(com)
        for j in range(i+1, len(coms)):
            nb += 1
            tmp = len(com.intersection(coms[j]))
            overlap += tmp
    mean_size = mean_size/len(coms)
    if nb>0:
        print("Overlap moyen constaté:"+str(overlap/(nb*mean_size))+" pour un groupe de taille moyenne "+str(mean_size))
    print("densité constatée: "+str(tire/(nb_node*nb_com)))
    print("There is "+str(len([0 for node in nodes if len(node) == 0]))+" nodes without link")
    print("There is "+str(len([0 for com in coms if len(com) == 0]))+" coms without link")

    affiliation_File = 'affiliation.txt'
    with open(affiliation_File, 'w') as output:
        for nID, node in enumerate(nodes):
            aff = ";".join((str(com) for com in node))
            output.write(str(nID)+" "+aff+"\n")

    str_dure = sys.argv[4]
    dure = makeIntervale(str_dure)
    str_act = sys.argv[5]
    act = makeIntervale(str_act)
    Tmax = float(sys.argv[6])
    activity_File = 'activity.txt'
    with open(activity_File, 'w') as output:
        for comID in range(0, nb_com):
            output.write(str(comID)+" ")
            cur_temp = 0
            activity_profile = []
            cur_duration = randInIntervale(dure)
            next_act = randInIntervale([0, Tmax-cur_duration])
            activity_profile.append(str(next_act)+",0")
            activity = randInIntervale(act)
            activity_profile.append(str(cur_duration)+","+str(activity))
            output.write(";".join(activity_profile)+"\n")
    print("Activy profile written in: "+activity_File)
    print("Affiliation written in: "+affiliation_File)
