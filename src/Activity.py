import bisect
import random
import math


# to have more info on how the simulation is done:
# http://www.r-bloggers.com/generating-a-non-homogeneous-poisson-process/


class Activity:
    """Compute the activity of a community"""

    def __init__(self, id):
        self.break_time = []  # list of time point at wich the rate change
        self.rate = []  # list of rate
        self.id = id

    def __str__(self):
        return "break:" + self.break_time.__str__() + "\n" +\
               "rate :" + self.rate.__str__() + "\n"

    def __add__(self, OtherA):
        self_i = 0
        other_i = 0
        third = Activity(-1)
        prev = 0
        while self_i < len(self.break_time) and other_i < len(OtherA.break_time):
            selfrate = self.rate[self_i]
            Orate = OtherA.rate[other_i]
            if self.break_time[self_i] < OtherA.break_time[other_i]:
                min_t = self.break_time[self_i]
                self_i += 1
            elif self.break_time[self_i] == OtherA.break_time[other_i]:
                min_t = OtherA.break_time[other_i]
                other_i += 1
                self_i += 1
            else:
                min_t = OtherA.break_time[other_i]
                other_i += 1
            third.addActivty(min_t-prev, selfrate+Orate)
            prev = min_t
        not_finished = self if self_i < len(self.break_time) else OtherA
        not_finished_i = self_i if self_i < len(self.break_time) else other_i
        while not_finished_i < len(not_finished.break_time):
            min_t = not_finished.break_time[not_finished_i]
            third.addActivty(min_t-prev, not_finished.rate[not_finished_i])
            not_finished_i += 1
            prev = min_t
        return third

    def addActivty(self, duration, rate):
        assert(len(self.break_time) == len(self.rate))
        self.rate.append(rate)
        past_time = self.break_time[-1] if self.break_time else 0
        self.break_time.append(past_time+duration)

    def at(self, t):
        """ return the current activity level at time t."""
        # Find i such that for all j<i, self.break_time[j] <=self.break_time[i]
        index = bisect.bisect(self.break_time, t)
        # if t is even after the last recorded activity return 0;
        res = self.rate[index] if index < len(self.rate) else 0
        print("At", t, "there is an activity level of", res)
        return res

    def integrationOver(self, beg, end):
        """Compute the mean activty between time beg and end"""
        index_beg = bisect.bisect(self.break_time, beg)
        index_end = bisect.bisect(self.break_time, end)
        time_span = [beg] + self.break_time[index_beg:index_end] + [end]
        mean = 0
        for i, t in enumerate(time_span[:-1]):
            delta = time_span[i+1] - t
            if index_beg < len(self.rate):
                mean += delta * self.rate[index_beg]
            index_beg += 1
        # print(time_span)
        # print("Integrale :", mean)
        return mean

    def invCumulativeDistrbution(self, beg, end, uniform):
        """ Compute a activation time betwwen beg and end such that:
            P(first_activation < t-beg )= uniform ."""
        born_inf = beg
        born_sup = end
        close_enough = False
        max_u = 1 - math.exp(- self.integrationOver(beg, born_sup))
        if max_u < uniform:
            # the next activation should be after end.
            # print("Last iteration should be after", end, "because :")
            # print("max is", max_u, "and", uniform, "wanted")
            return end+1, -1
        while not close_enough:
            middle = (born_inf + born_sup) / 2.0
            mean_expo = self.integrationOver(beg, middle)
            proba = 1 - math.exp(-mean_expo)

            if proba > uniform:
                born_sup = middle
            else:
                born_inf = middle
            if abs(proba - uniform) < 0.001:
                close_enough = True
        middle = (born_inf + born_sup) / 2.0
        return middle, proba

    def activationBetween(self, beg, end):
        """ return a realisation of activations between beg and end."""
        activation = []
        cur = beg
        TMAX = end
        while cur < TMAX:
            uniform = random.random()
            next_act, prob = self.invCumulativeDistrbution(cur, TMAX, uniform)
            inter = next_act-cur
            cur = next_act
            if next_act < TMAX:
                activation.append(cur)
        return activation


if __name__ == '__main__':
    com1 = Activity(1)
    com1.addActivty(10, 3)
    com1.addActivty(5, 1)
    com1.addActivty(3, 40)
    com1.addActivty(10, 12)

    activation = com1.activationBetween(5, 16)
    print(len(activation))
    print(com1)
    plt.scatter(activation, [4]*len(activation))
    plt.show()
    plt.close()
