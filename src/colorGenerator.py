import random


class Color:

    def __init__(self, r, g, b):
        self.tab = [r, g, b]
        self.minColor = 50
        self.maxColor = 200

    @staticmethod
    def from_tab(tab):
        obj = Color(0, 0, 0)
        obj.tab = tab
        return obj

    def __str__(self):
        return "rgb(" + ", ".join((str(x) for x in self.tab)) + ")"

    def offsetColor(self, offset):
        """Take a quadruplet(r,g,b) and return the samecolor offseted\
     offset should be a number between [-1,1] """
        assert(offset > -256)
        assert(offset < 256)
        return Color.from_tab([max(self.minColor, min(self.maxColor, x + offset)) for x in self.tab])


class colorGenerator:
    """generates a rgb Colors for a given element"""

    def __init__(self):
        self.lookUp = dict()  # store the color given to an element

    def impose(element, color):
        """ impose that element has color as color"""
        self.lookUp[element] = color

    def generate(self, cur):
        if cur not in self.lookUp:
            color = Color(self.__rand(), self.__rand(), self.__rand())
            self.lookUp[cur] = color
        return self.lookUp[cur]

    def get(self, cur):
        return str(self.generate(cur))

    def __rand(self):
        return round(56 + (random.random()*200))
